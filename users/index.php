<?php


use Phalcon\Mvc\Micro;
use Phalcon\Loader;
use Phalcon\Di\FactoryDefault;
use Phalcon\Session\Adapter\Files as Session;
use Phalcon\Db\Adapter\Pdo\Sqlite ;
use Phalcon\Http\Request;


try {

    $config = include __DIR__ . '/app/config/config.php';
    $di = new FactoryDefault();
    $di->set('db', function () use ($config) {
        return new Sqlite(
            [
                "dbname" => $config->database->name
            ]
        );
    });

    $di->setShared(
        'session',
        function () {
            $session = new Session();
            $session->start();
            return $session;
        }
    );

    $loader = new Loader();

    $loader->registerDirs([$config->application->modelsDir,
        $config->application->controllersDir,
        $config->application->migrationsDir,
        $config->application->viewsDir,
        $config->application->helpersDir,
    ])->register();

    $app = new Micro($di);

    Helper::router($app);

    $app->notFound(function () use ($app) {
        echo 404;
    });
    $app->handle();

} catch (\Exception $e) {

    echo $e->getMessage(), PHP_EOL;
    echo $e->getTraceAsString();
}

