<?php

use Phalcon\Config;

return new Config(
    [
        'database'    => [
            'name'     => '/var/www/html/db/phalcon.sqlite3',
            'dbname'     => '/var/www/html/db/phalcon.sqlite3',
            'adapter'  => 'Sqlite'
        ],
        'application' => [
            'modelsDir' =>  'app/models/',
            'migrationsDir' =>  'app/migrations/',
            'controllersDir' => 'app/controllers/',
            'commandsDir' => 'app/commands/',
            'viewsDir' => 'app/views/',
            'helpersDir' => 'app/helpers/',
            'baseUri'   => '/',
        ],
        'models'      => [
            'metadata' => [
                'adapter' => 'Memory'
            ]
        ]
    ]
);
