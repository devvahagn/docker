<?php

use Phalcon\Mvc\Controller;

class SessionController extends Controller
{

    public function loginAction()
    {

        if (!$this->request->isPost()) {
            return;
        }

        Helper::login($this);

    }

    public function logoutAction()
    {
        $this->session->destroy();
        $this->response->setStatusCode(200, '');
        $this->response->setContent(json_encode(['result' => '','jsonrpc'=>$this->request->getPost('jsonrpc'),'id'=>$this->request->getPost('id')]));
        $this->response->send();
    }
}