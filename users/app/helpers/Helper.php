<?php

use Phalcon\Http\Response;
use Phalcon\Http\Request;
use Phalcon\Security;

class Helper
{

    public static function router($app)
    {

        if ($app->request->getRawBody()) {

            $request = json_decode($app->request->getRawBody());

            if (isset($request->jsonrpc) && $request->jsonrpc == '2.0') {

                $method = explode('_', $request->method);
                $str = $method[0];
                $class = $str . 'Controller';
                $controller = new $class();
                try {
                    $app->map(
                        "/",
                        [
                            $controller,
                            $method[1] . "Action"
                        ]
                    );
                } catch (\Exception $e) {}
            }
        }else{
            $app->response->setStatusCode(501, 'Not Found');
            $app->response->setContent(json_encode(['result' => 'error', 'jsonrpc' => '2.0', 'id' => $request->id]));
            $app->response->send();
        }
    }


    public function login($arg)
    {

        $data = json_decode($arg->request->getRawBody());

        $user = Users::findFirstByLogin($data->params->login);

        $result = 'неверный логин или пароль';
        $status = 403;
        if ($user) {

            if ($arg->security->checkHash($data->params->password, $user->password)) {

                $result = $arg->security->hash($user->login);
                $arg->session->set("token", $user->login);
                $status = 200;

            } else {
                $result = 'неверный логин или пароль';
                $status = 403;
            }
        }

        $arg->response->setStatusCode($status, '');
        $arg->response->setContent(json_encode(['result' => $result, 'jsonrpc' => '2.0', 'id' => $data->id]));
        $arg->response->send();
    }
}